Feature: PPI test script

Background:
* url ppiurl
* def response1 = read ('ppiAuth.feature')
* def result = call response1
* def ppidata = read ('data/ppiData.json')
* header Authorization = 'Bearer ' +  result.response.token

Scenario: PPI get all ProductList of the api using authorization key
Given path '/getProductList'
When method get
Then status 200
* print 'Get all ProductList response is', response

Scenario: PPI Customer Master list
Given path '/getCustomerMasterList'
And params {lang:'en'}
When method get
Then status 200
* print 'Customer Master list response is', response

Scenario: PPI View Customer Details
Given path '/viewCustomerDetails/15570'
And params {lang:'en'}
When method get
Then status 200
* print 'View customer details response is', response

Scenario: PPI View Policy Details
Given path '/viewPolicyDetails/14379'
And params {lang:'en'}
When method get
Then status 200
* print 'PPI View Policy Details response is', response

Scenario: PPI List Document Details
Given path '/listDocument'
And params {lang:'en', claimId:415}
When method get
Then status 200
* print 'PPI List Document Details response is', response

Scenario: PPI Get Product List Details
Given path '/getProductList'
When method get
Then status 200
* print 'PPI Get Product List response is', response

Scenario: PPI Get Claim Cause List
Given path '/getClaimCauseList'
And params {lang:'en', parentId:181}
When method get
Then status 200
* print 'PPI Get Claim Cause List response is', response

Scenario: PPI Get Claim Master List
Given path '/getClaimMasterList'
And params {lang:'en', productCategory:'HomeContent'}
When method get
Then status 200
* print 'PPI Get Claim Master List response is', response

Scenario: PPI get Claim List
Given path '/getClaimList/14364'
And params {lang:'en'}
When method get
Then status 200
* print 'PPI get Claim List response is', response

Scenario: PPI get Claim Details
Given path '/claims/431'
And params {lang:'en'}
When method get
Then status 200
* print 'PPI get Claim Details response is', response

Scenario: PPI Search Customer
Given path '/searchCustomer'
And params {src:'en', lang:'lang', page_no:1, limit:20}
And request ppidata[1]
When method POST
Then status 200
* print 'Search Customer response is', response

Scenario: PPI Update Customer Details
Given path '/updateCustomer'
And params {lang:'en'}
And request ppidata[2]
When method POST
Then status 200
* print 'Update customer details response is', response

Scenario: PPI Policy Search Details
Given path '/getPolicySearch'
And params {src:'CRM', lang:'en', page_no:1, limit:20}
And request ppidata[3]
When method POST
Then status 200
* print 'PPI Policy Search response is', response

Scenario: PPI Get Report Details
Given path '/getReport'
And request ppidata[4]
When method POST
Then status 200
* print 'PPI Get Report response is', response

Scenario: PPI get Claim Search Details
Given path '/getClaimSearch'
And params {src:'CRM', lang:'en', page_no:1, limit:20}
And request ppidata[5]
When method POST
Then status 200
* print 'PPI get Claim Search response is', response


