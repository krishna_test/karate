Feature: Create Authorization for the HCPH Client

Background:
* def postSET = read ('data/setPostBody.json')
* def headerpart = { bolt-country-code:'TW', bolt-tenant-id:'056a7ab4-affa-4ad0-b0ac-9bae4516b615', Accept:'*/*', Content-Type:'application/json' }

Scenario: Get access token
* url 'https://identity.csdocker.staging.device.bolttech.asia/'
Given path 'identity/oauth/token'
And headers headerpart
And request postSET
When method POST
Then status 200
* def accessToken = response.access_token
* print 'The access token from SETTW is ', accessToken