Feature: HCPH Create Customer, Get Contract, Get order, Get Product

Background:
* url urlBase
* print 'url base is', urlBase
* def headerpart2 = { bolt-country-code:'PH', bolt-tenant-id:'74ade54e-7146-428d-8f41-486f3eb8de82',bolt-language-code:'EN', Accept:'*/*', Content-Type:'application/json' }
* def responsedata2 = read ('HCPHAuth.feature')
* def result3 = call responsedata2
* header Authorization = 'Bearer ' + result3.response.access_token
* def createcustomer = read ('data/hcCreateCustomer.json')
* def getcontract = read ('data/hcGetContract.json')

Scenario: Create customer for HCPH
Given path '/v1/order/create-customer-fulfill'
And headers headerpart2
And request createcustomer
When method POST
Then status 200
* print 'The response of order from create customer is', response.order_status

Scenario: Get Contract of HCPH
Given path '/v1/contract/service_contract/list'
And headers headerpart2
And request getcontract
When method POST
Then status 200
* print 'Get contract API response is', response.result
Then match response._status.error contains ['Success']

Scenario: Get HCPH Order
Given path '/v1/order'
And params {order_id:851533057550, partner_code:'PHHOMCRCFINA01'}
And headers headerpart2
When method Get
Then status 200
* print 'The response from get order', response._status
Then match response._status.message contains 'Success'

Scenario: Get HCPH Product by RRP and ProductCode
Given path '/v1/products'
And params {device_rrp:1000,partner_code:'PHHOMCRCFINA01'}
And headers headerpart2
When method Get
Then status 200
* print 'The response from the Get HCPH Product is', response
Then match response._status.message contains 'Success'