Feature: To test create, login and access quote using jwt

Background:
* def postBody = read ('data/ppiData.json')
* url 'http://18.141.34.115:7050'

Scenario: Get access token
Given path '/userPortal/getPermissionList'
And request postBody[0]
When method POST
Then status 200
* def accessToken = response.token
* print 'the access token is ', accessToken