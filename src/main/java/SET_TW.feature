Feature: SET TW Create Customer, Get Contract, Get order, Get Product

  Background: 
    * url 'https://api.staging.device.bolttech.asia'
    * def headerpart = { bolt-country-code:'TW', bolt-tenant-id:'056a7ab4-affa-4ad0-b0ac-9bae4516b615',bolt-language-code:'EN', Accept:'*/*', Content-Type:'application/json' }
    * def createcustomer = read ('data/setCreateCustomer.json')
    * def responsedata = read ('SETTWAuth.feature')
    * def getfulfill = read ('data/fulFill.json')
    * def getprefill = read ('data/preFill.json')
    * def result2 = call responsedata
    * header Authorization = 'Bearer ' +  result2.response.access_token

  Scenario: Create customer for SETTW
    Given path '/v1/order/order-with-customer'
    And headers headerpart
    And request createcustomer
    When method POST
    Then status 200
    * def ordernumber = response.order_id
    * print 'Get contract API response is', response.ordernumber
    * print 'The response of order from create customer is', response.order_status

 		Given path '/v1/order/'+ordernumber+'/fulfill'
    * def result3 = call responsedata
    * header Authorization = 'Bearer ' +  result3.response.access_token
    And headers headerpart
    And request getfulfill
    When method POST
    Then status 200
    * print 'The response from the Get SETTW Product is', response
    Then match response._status.message contains 'Success'
    
  Scenario: Prefill order for SETTW
    Given path '/v1/integration/order/prefill'
    And headers headerpart
    And request getprefill
    When method POST
    Then status 200

  Scenario: Get prefill Order
    Given path '/v1/integration/order/prefill/352123113311364'
    And params {partner_code:'TWSAMSUOEMNA01'}
    And headers headerpart
    When method Get
    Then status 200
    * print 'The response from get order', response._status
    Then match response._status.message contains 'success'
