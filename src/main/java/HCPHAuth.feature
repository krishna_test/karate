Feature: Create Authorization for the HCPH Client

Background:
* def postHC = read ('data/hcPostBody.json')
* def headerpart = { bolt-country-code:'PH', bolt-tenant-id:'74ade54e-7146-428d-8f41-486f3eb8de82', Accept:'*/*', Content-Type:'application/json' }

Scenario: Get access token
* url 'https://identity.csdocker.staging.device.bolttech.asia/'
Given path 'identity/oauth/token'
And headers headerpart
And request postHC
When method POST
Then status 200
* def accessToken = response.access_token
* print 'The access token from HCPH is ', accessToken